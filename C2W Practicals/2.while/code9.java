//que9.write a program to reverse a given number.
//
//input:  642111423
//output: 324111346



import java.util.Scanner;
class Reverse{
        public static void main(String args[]){
                Scanner sc = new Scanner (System.in);

                int n = sc.nextInt();
                      
		while(n!=0){
			int i = n%10;
			System.out.print(i);
			n = n/10;
		}

        }
}

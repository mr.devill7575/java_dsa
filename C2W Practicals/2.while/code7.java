//que7.print the reverse number  
//if number is even and 
//if number is odd then print odd number only
//
//input: 6
//output:654321
//
//input: 7
//output: 7531

import java.util.*;
class Oddeven{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in); 
		
		int n = sc.nextInt();
		if (n%2==0){
			while(n!=0){
				System.out.print(n);
				n--;
			}
			}
			else{
				while(n>=0){
					System.out.print(n);
					n= n-2;
				}
			}
		}
	}


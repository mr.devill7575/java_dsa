//que8.write a program to print the countdown of days to submit the assignment 
//
//input: day=7
//output: 7 days remainning 
//        6 days remainning
//        5 days remainning
//        4 days remainning
//        3 days remainning
//        2 days remainning
//        1 days remainning
//        0 days assignment is overdue

import java.util.Scanner;
class Assignment{
	public static void main(String args[]){
		Scanner sc = new Scanner (System.in);

		int n = sc.nextInt();
			int i = 0;
		while(n>=i){
			if (n!=0){
				
			System.out.println(n+" days remaining");
			
			}
			else{
				  System.out.println(n+" days assignment is overdue");
			}
			n--;

		}
		
	}
}

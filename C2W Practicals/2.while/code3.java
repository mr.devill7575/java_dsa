//que3.count the digit of given number 
//input: n=942111423
//output: 9
 
import java.util.*;
class Count{
        public static void main(String bp[]){
                        Scanner sc = new Scanner (System.in);
                                int n = sc.nextInt();
           				int count = 0;
				while(n!=0){
					int i = n%10;
					count++;
					n = n/10;
				}
				 System.out.println(count);
        }
}

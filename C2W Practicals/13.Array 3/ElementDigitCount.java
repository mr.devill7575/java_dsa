/*Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
*/
import java.io.*;
class ElementDigitCount {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enetr Total number of Elements :");
                int num = Integer.parseInt(br.readLine());
                int arr[] = new int[num];
                System.out.println("Enetr Elements :");
                for(int i=0; i<arr.length; i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                for(int i=0; i<arr.length; i++) {
                        int x=arr[i];
                        int count =0;
                        while(x!=0) {
                                count++;
                                x=x/10;
                        }
                        System.out.print(count+" ");
                }
        }
}
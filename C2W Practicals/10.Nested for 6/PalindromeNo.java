/*
 Write a program to take range as input from the user and print Palindrome numbers. ( Take a start and
end number from a user )
Input: Enter start: 100
Enter end: 250
Output: Palindrome numbers between 100 and 250
101 111 121 131 141 151 161 171 181 191 202 212 222
*/
import java.io.*;
class PalindromeNo {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter start limit");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter end limit");
                int end =  Integer.parseInt(br.readLine());

                for(int i=start; i<=end; i++) {
                        int num = i;
                        int rev =0;
                        while(num!=0) {
                                int rem = num%10;
                                rev = rev*10+rem;
                                num = num/10;
                        }
                        if(rev==i) {
                                System.out.print(rev+" ");
                        }
                }
        }
}

//que 1. write the java program that check if a number is even or odd.
//
//input1: var = 10;
//output: 10 is an even no
//
//input1: var = 37;
//output: 37 is an odd no 
//
//input1: var = 0;
//output: ???????


import java.util.*;
class Evenodd{
  	public static void main (String args[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter the no. :- ");
		int var = sc.nextInt();

		if(var%2==0){
			System.out.println(var+" is even");
		}
		else{
			 System.out.println(var+" is odd");
		}
	}
}

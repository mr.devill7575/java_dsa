class MyThread extends Thread {

        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }

        public void run(){
                System.out.println(Thread.currentThread());
        }
}
class ThreadGroupDemo5 {
        public static void main(String args []) {

                ThreadGroup pthreadgp = new ThreadGroup ("Core2Web");

                MyThread obj1 = new MyThread(pthreadgp,"Java");
                obj1.start();
                MyThread obj2 = new MyThread(pthreadgp,"C");
                obj2.start();
                MyThread obj3 = new MyThread(pthreadgp,"CPP");
                obj3.start();
		
		ThreadGroup cthreadgp = new ThreadGroup (pthreadgp,"Core2Web");
		
		MyThread obj4 = new MyThread(cthreadgp,"Flutter");
                obj4.start();
                MyThread obj5 = new MyThread(cthreadgp,"ReactJS");
                obj5.start();
                MyThread obj6 = new MyThread(cthreadgp,"SpringBoot");
                obj6.start();
        }

}
/*
 * Sequence will be change each time
Thread[Java,5,Core2Web]
Thread[CPP,5,Core2Web]
Thread[Flutter,5,Core2Web]
Thread[C,5,Core2Web]
Thread[SpringBoot,5,Core2Web]
Thread[ReactJS,5,Core2Web]
*/

class MyThread extends Thread {

        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }
	
	MyThread(){
		super();
	}

        public void run(){
                System.out.println(Thread.currentThread());
        }
}
class ThreadGroupDemo3 {
        public static void main(String args []) {

                ThreadGroup pthreadgp = new ThreadGroup ("Core2Web");

                MyThread obj1 = new MyThread(pthreadgp,"Java");
                obj1.start();
                MyThread obj2 = new MyThread(pthreadgp,"DSA");
                obj2.start();
                MyThread obj3 = new MyThread();
                obj3.start();
        }

}
/*
Thread[Java,5,Core2Web]
Thread[Thread-0,5,main]
Thread[DSA,5,Core2Web]
*/

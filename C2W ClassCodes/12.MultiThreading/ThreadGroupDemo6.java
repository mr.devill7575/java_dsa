class MyThread extends Thread {

        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }

        public void run(){
                System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);                  //sleep() method for all 6 threads
		}catch(InterruptedException ie) {
			System.out.println(ie.toString());
		}	
        }
}
class ThreadGroupDemo6 {
        public static void main(String args []) throws InterruptedException{

                ThreadGroup pthreadgp = new ThreadGroup ("India");

                MyThread t1 = new MyThread(pthreadgp,"Mah");
                MyThread t2 = new MyThread(pthreadgp,"Goa");
                
		t1.start();
                t2.start();

		ThreadGroup cthreadgp = new ThreadGroup (pthreadgp,"Pakistan");

                MyThread t3 = new MyThread(cthreadgp,"Karachi");
                MyThread t4 = new MyThread(cthreadgp,"Lahore");

                t3.start();
                t4.start();

		ThreadGroup cthreadgp2 = new ThreadGroup (pthreadgp,"Bangladesh");

                MyThread t5 = new MyThread(cthreadgp2,"Mirpur");
                MyThread t6 = new MyThread(cthreadgp2,"Dhaka");

                t5.start();
                t6.start();
		
		cthreadgp.interrupt();			// it sets the interrupted status = true, which does not allow sleep/wait/join method, if calls then throws exception.

		System.out.println(pthreadgp.activeCount());
		System.out.println(pthreadgp.activeGroupCount());
        }

}


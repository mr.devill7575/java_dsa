class MyThread extends Thread {

        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }

        public void run(){
                System.out.println(Thread.currentThread());
        }
}
class ThreadGroupDemo2 {
        public static void main(String args []) {
		
                ThreadGroup pthreadgp = new ThreadGroup ("Core2Web");
		
		MyThread obj1 = new MyThread(pthreadgp,"Java");
                obj1.start();
		MyThread obj2 = new MyThread(pthreadgp,"DSA");
                obj2.start();
		MyThread obj3 = new MyThread(pthreadgp,"Flutter");
                obj3.start();
        }

}
/*OUTPUT-
 Thread[Java,5,Core2Web]
 Thread[Flutter,5,Core2Web]
 Thread[DSA,5,Core2Web]
*/

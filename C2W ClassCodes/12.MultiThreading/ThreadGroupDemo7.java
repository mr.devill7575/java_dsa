class MyThread implements Runnable {

        public void run(){
                System.out.println(Thread.currentThread());
                try{
                        Thread.sleep(5000);
                }catch(InterruptedException ie) {
                        System.out.println(ie.toString());
                }
        }
}
class ThreadGroupDemo7{
   	public static void main(String args []) throws InterruptedException{

                ThreadGroup pthreadgp = new ThreadGroup ("India");

                MyThread obj1 = new MyThread();
                MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pthreadgp,obj1,"Maharashtra");
		Thread t2 = new Thread(pthreadgp,obj2,"Goa");

                t1.start();
                t2.start();
	}

}

class MyThread extends Thread {
        public void run () {
                
              System.out.println("In Run "+Thread.currentThread().getPriority());
        }
}
class ThreadPriorityDemo {
        public static void main(String args[]){

		Thread t = new Thread ();

                MyThread obj = new MyThread();
		obj.start();

		t.setPriority(7);

		MyThread obj1 = new MyThread();
		obj1.start();

      		System.out.println("In Main "+Thread.currentThread().getPriority());
                
        }
}


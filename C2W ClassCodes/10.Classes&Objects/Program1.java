class Player {
	private String name = null;
	
	String str1 = "Virat";
	String str2 = "MSD";
	String str3 = "Rohit";

	String str4 = new String("Virat");
	String str5 = new String("MSD");
	String str6 = new String("Rohit");

	Player(String name){
		this.name = name;
	}
	void info () {
		System.out.println(System.identityHashCode(name));
	}
	void demo () {
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	}
	void demo2 () {
		System.out.println(System.identityHashCode(str4));
                System.out.println(System.identityHashCode(str5));
                System.out.println(System.identityHashCode(str6));
	}
}
class Client {
	public static void main (String args []) {
		Player obj1 = new Player("Virat");
		obj1.info();

		Player obj2 = new Player("MSD");
                obj2.info();

		Player obj3 = new Player("Rohit");
		obj3.info();
		
		System.out.println();

		obj3.demo();

		System.out.println();

		obj3.demo2();
	}
}



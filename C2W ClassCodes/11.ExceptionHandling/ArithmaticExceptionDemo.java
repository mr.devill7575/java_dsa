import java.util.Scanner;

public class ArithmeticExceptionDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a dividend: ");
        int dividend = scanner.nextInt();

        System.out.print("Enter a divisor: ");
        int divisor = scanner.nextInt();

        try {
            int quotient = divide(dividend, divisor);
            System.out.println("Quotient: " + quotient);
        } catch (ArithmeticException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

    public static int divide(int dividend, int divisor) {
        return dividend / divisor;
    }
}


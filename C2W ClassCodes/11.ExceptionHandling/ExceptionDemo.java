import java.util.*;
class IncorrectMobileNoException extends RuntimeException {
	IncorrectMobileNoException( String msg ) {
		super( msg );
	}
}
class ExceptionDemo {
	public static void main (String args[]){
		System.out.println("Enter 10 Digit Mobile Number :");
		Scanner sc = new Scanner (System.in);
		String data = sc.next();
		int size = data.length();
		if(size>10||size<10){
			throw new IncorrectMobileNoException("Incorrect Mobile Number");
		}
		System.out.println("Mobile Number= "+data);

	}
}


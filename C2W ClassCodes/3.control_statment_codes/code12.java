//QUE12 take an integer n as input print multiple of 4 till n 
//input: 22
//output:4 8 12 16 20
import java.util.*;
class Multiple{
	public static void main (String args[]){
		Scanner sc = new Scanner (System.in);
			int i = 1;
			System.out.print("enter the value of the n :- ");
			int n = sc.nextInt();

			while(i<=n){
				if (i%4==0){
					System.out.print(i+" ");

				}
					i++;
			}

	}
}

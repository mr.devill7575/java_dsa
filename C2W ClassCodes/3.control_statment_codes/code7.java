//given an integer value as input
//:print "fizz" if it divisible by 3
//:print "buzz" if it divisible by 5
//:print "fizz-buzz" if it divisible by 3
//:if not then print "not divisible by both"

import java.util.*;

class Fizz{
	public static void main(String args[]){
		Scanner sc = new Scanner (System.in);

		       int n;
		System.out.print("enter the n :- ");
        	   n = sc.nextInt(); 
		   
		if (n%5==0 && n%3==0){
		      System.out.println("fizz - buzz");
		}   
		else if (n%5==0){
                      System.out.println("buzz");
                }
		else if (n%3==0){
                      System.out.println("fizz");
                }
		else{
			System.out.println("not divisible by both");
		}
	}
}

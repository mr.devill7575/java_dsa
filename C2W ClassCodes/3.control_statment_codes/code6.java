//given a integer print whether  it odd or even
//
//input: 7
//output: 7 is odd
//
//input: 4
//output: 4 is even


class Oddeven{
	public static void main (String args []){
		int n = 4;
		
	     if (n%2==0){
	     	System.out.println(n+" is even");
	     }	

	     else {
	     	 System.out.println(n+" is odd");
	     }
	}
}
